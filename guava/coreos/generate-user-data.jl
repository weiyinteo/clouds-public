#!/usr/bin/julia

import JSON

settings = {
  "coreos3" => {
    "replacements" => [
      ( "{{HOSTNAME}}", "coreos3" ),
      ( "{{COREOS_PUBLIC_IP}}", "192.168.1.53" ),
      ( "{{COREOS_PRIVATE_IP}}", "192.168.1.53" ),
      ( "{{ETC_TOKEN}}", "48dfe38b0eacf930c8abbbf614463828" )
    ],
    "units" => [ 
      {
        "name" => "etcd.service",
        "command" => "start",
        "content" => "no"
      },
      {
        "name" => "fleet.service",
        "command" => "start",
        "content" => "no"
      },
      {
        "name" => "database-data.service",
        "command" => "start",
        "enable" => "true"
      },
      {
        "name" => "database.service",
        "command" => "start",
        "enable" => "true"
      },
      {
        "name" => "db-backup.service",
        "enable" => "true"
      },
    ],
  },
  "coreos4" => {
    "replacements" => [
      ( "{{HOSTNAME}}", "coreos4" ),
      ( "{{COREOS_PUBLIC_IP}}", "192.168.1.54" ),
      ( "{{COREOS_PRIVATE_IP}}", "192.168.1.54" ),
      ( "{{ETC_TOKEN}}", "6cfbfc750d76707fc4b7ea16dfa8ef11" )
    ],
    "units" => [ {
      "name" => "wp.service",
      "command" => "start",
      "enable" => "true"
    }],
    "unitFiles" => [ "kubernetes-units.cfg", "kubernetes-master-units.cfg" ]
  },
  "kmaster" => {
    "replacements" => [
      ( "{{HOSTNAME}}", "kmaster" ),
      ( "{{COREOS_PUBLIC_IP}}", "192.168.1.55" ),
      ( "{{COREOS_PRIVATE_IP}}", "192.168.1.55" ),
      ( "{{ETC_TOKEN}}", "94ffba1c39bbe0a1858dd6d6318f9cb6" )
    ],
    "units" => [ {
    }],
    "unitFiles" => [ "kubernetes-units.cfg", "kubernetes-master-units.cfg" ]
  },
  "kmin1" => {
    "replacements" => [
      ( "{{HOSTNAME}}", "kmin1" ),
      ( "{{COREOS_PUBLIC_IP}}", "192.168.1.56" ),
      ( "{{COREOS_PRIVATE_IP}}", "192.168.1.56" ),
      ( "{{CLUSTER_MASTER_PRIVATE_IP}}", "192.168.1.55" ),
      ( "{{ETC_TOKEN}}", "94ffba1c39bbe0a1858dd6d6318f9cb6" )
    ],
    "units" => [ {
    }],
    "unitFiles" => [ "kubernetes-node-units.cfg", "kubernetes-units.cfg" ]
  },
  "kmin2" => {
    "replacements" => [
      ( "{{HOSTNAME}}", "kmin2" ),
      ( "{{COREOS_PUBLIC_IP}}", "192.168.1.57" ),
      ( "{{COREOS_PRIVATE_IP}}", "192.168.1.57" ),
      ( "{{CLUSTER_MASTER_PRIVATE_IP}}", "192.168.1.55" ),
      ( "{{ETC_TOKEN}}", "94ffba1c39bbe0a1858dd6d6318f9cb6" )
    ],
    "units" => [ {
    }],
    "unitFiles" => [ "kubernetes-units.cfg" ]
  }
}

function includeFile(host, path, prefix)
  replacements = get(get(settings, host, {}), "replacements", {})
  template = open(path)
  for line in eachline(template)
    for replacement in replacements
      line = replace(line, replacement[1], replacement[2])
    end
    print("$prefix$line")
  end
end

function generateUnits(host, units)
  # load units
  for unitData in units
    name = unitData["name"]
    println("   - name: $name")

    command = get(unitData, "command", "")
    if command != ""
      println("     command: $command")
    end

    enable = get(unitData, "enable", "")
    if enable != ""
      println("     enable: $enable")
    end

    content = get(unitData, "content", "yes")
    if content == "yes"
      fileName = get(unitData, "file", name)
      println("     content: |")
      includeFile(host, joinpath("units", fileName), "        ")
    end
  end
end 

function generateWriteFiles(host)
  # write_files
  println("write_files:");
  for fileData in JSON.parsefile("file-list.cfg")
    path=fileData["path"]
    println("  - path: $path")

    owner= get(fileData,"owner", "")
    if owner != ""
      println("    owner: '$owner'")
    end

    permissions= get(fileData,"permissions", "")
    if permissions != ""
      println("    permissions: '$permissions'")
    end

    println("    content: |")
    includeFile(host, joinpath("files", fileData["name"]), "        ")
  end
end

function generate(host)
  includeFile(host, "user_data.template", "")

  # load common units
  commonUnits = JSON.parsefile("common-units.cfg")
  generateUnits(host, commonUnits)

  hostSettings = get(settings, host, {})
  units = get(hostSettings, "units", [])
  generateUnits(host, units)

  unitFiles = get(hostSettings, "unitFiles", [])
  for unitFile in unitFiles
    units = JSON.parsefile(unitFile)
    generateUnits(host, units)
  end

  generateWriteFiles(host)
end

generate(ARGS[1])
